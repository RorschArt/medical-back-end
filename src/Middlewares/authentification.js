const jwt  = require('jsonwebtoken')
const User = require('../models/user')

// Authentification User
const auth = async (req, res, next) => {

    try {
        const json = req.header('Authorization').replace('Bearer ', '')
        //Contient l'_id
        const jsonDecoded = jwt.verify(json, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: jsonDecoded._id, 'tokens.token': json })

        !user ? res.status(401).send() : false

        // On stocke nos valeur dans la req pour l'utiliser dans le route (performance)
        req.token = json
        req.user = user

        user.role.roleName === 'admin' ? req.user.isAdmin = true : req.user.isAdmin = false

        next()
    } catch (e) {
        res.status(401).send({ erreur: 'SVP authentifiez-vous !' })
    }
}

module.exports = auth
