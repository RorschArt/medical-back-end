// Npm
const mongoose      = require('mongoose')

const imageArticleSchema = new mongoose.Schema({

        image: {
            type: Buffer
        }
    }, { timestamps: true }
)
const ImageArticle = mongoose.model('ImageArticle', imageArticleSchema)

module.exports = ImageArticle
