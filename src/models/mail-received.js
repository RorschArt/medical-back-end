// Middleware
const auth      = require('../Middlewares/authentification')
// Npm
const mongoose  = require('mongoose')

const mailReceivedSchema = new mongoose.Schema({
        header: {
            type: String,
            trim: true,
            required: true
        },
        textBody: {
            type: String,
            trim: true,
            required: true
        },
        from: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Role',
            autopopulate: true
        },
        to: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            required: true,
            autopopulate: true
        },
        unRead: {
          type: Boolean,
          default: true
        }
    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
mailReceivedSchema.post('save', async function (mailDoc) {
    await mailDoc.populate('to').populate('from').execPopulate()
})

 mailReceivedSchema.plugin(require('mongoose-autopopulate'))

const MailReceived = mongoose.model('Mail-received', mailReceivedSchema)

module.exports = MailReceived
