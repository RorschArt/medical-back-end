const mongoose = require('mongoose')

const timetableSchema = new mongoose.Schema({
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        unique: true,
        ref: 'Role',
        required: true,
        autopopulate: true
    }
    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
timetableSchema.post('save', async function (timetableDoc) {
    await timetableDoc.populate('owner').execPopulate()
})

timetableSchema.plugin(require('mongoose-autopopulate'))

const Timetable = mongoose.model('Timetable', timetableSchema)

module.exports = Timetable