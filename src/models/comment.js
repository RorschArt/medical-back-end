// Npm
const mongoose      = require('mongoose')

const commentSchema = new mongoose.Schema({
        textBodyComment: {
            type: String,
            trim: true,
            required: true
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            required: true,
            autopopulate: true
        },
        articleFrom: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Article',
            required: true,
            autopopulate: true
        }

    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
commentSchema.post('save', async function (commentDoc) {
    await commentDoc.populate('author').populate('articleFrom').execPopulate()
})
//Post Hook (findOneAndDelete) -> populate author et articleFrom
commentSchema.post('findOneAndDelete', async function(commentDoc) {
    await commentDoc.populate('author').populate('articleFrom').execPopulate()
})

commentSchema.plugin(require('mongoose-autopopulate'))

const Comment = mongoose.model('Comment', commentSchema)

module.exports = Comment