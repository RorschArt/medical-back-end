// Npm
const mongoose = require('mongoose')

const mailSavedSchema = new mongoose.Schema({
        header: {
            type: String,
            trim: true,
            required: true
        },
        textBody: {
            type: String,
            trim: true,
            required: true
        },
        from: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Role',
            autopopulate: true
        },
        to: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            autopopulate: true
        }
    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
mailSavedSchema.post('save', async function (mailDoc) {
    await mailDoc.populate('to').populate('from').execPopulate()
})

 mailSavedSchema.plugin(require('mongoose-autopopulate'))

const MailSaved = mongoose.model('Mail-save', mailSavedSchema)

module.exports = MailSaved