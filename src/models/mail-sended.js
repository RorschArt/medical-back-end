// Middleware
const auth     = require('../Middlewares/authentification')
// Npm
const mongoose = require('mongoose')

const mailSendedSchema = new mongoose.Schema({
        header: {
            type: String,
            trim: true,
            required: true
        },
        textBody: {
            type: String,
            trim: true,
            required: true
        },
        from: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Role',
            autopopulate: true
        },
        to: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            required: true,
            autopopulate: true
        }
    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
mailSendedSchema.post('save', async function (mailDoc) {
    await mailDoc.populate('to').populate('from').execPopulate()
})

 mailSendedSchema.plugin(require('mongoose-autopopulate'))

const MailSended = mongoose.model('Mail-send', mailSendedSchema)

module.exports = MailSended