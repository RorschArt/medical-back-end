const mongoose       = require('mongoose')

const activitySchema = new mongoose.Schema({
        name: {
            type: String,
            trim: true
        },
        timestamp: {
            unique: true,
            type: Number,
            required: true,
            trim: true
        },
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            required: true,
            autopopulate: true
        }
    }, { timestamps: true }
)

//Post Hook (save) -> populate 'from' et 'to'
activitySchema.post('save', async function (activityDoc) {
    await activityDoc.populate('owner').execPopulate()
})

activitySchema.plugin(require('mongoose-autopopulate'))

const Activity = mongoose.model('Activity', activitySchema)

module.exports = Activity
