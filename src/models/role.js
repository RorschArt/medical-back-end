// Npm
const mongoose   = require('mongoose')
const validator  = require('validator')

const roleSchema = new mongoose.Schema({
        roleName: {
            type: String,
            trim: true,
            required: true
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
            autopopulate: true
        }
    }, {timestamp: true}
)

// Lien VIRTUEL mail Envoyé
roleSchema.virtual('mailsSended', {
    ref: 'Mail-send',
    localField: '_id',
    foreignField: 'from'
})
// Lien VIRTUEL mail Reçu
roleSchema.virtual('mailsReceived', {
    ref: 'Mail-received',
    localField: '_id',
    foreignField: 'to'
})
// Lien VIRTUEL mail Reçu
roleSchema.virtual('mailsSaved', {
    ref: 'Mail-save',
    localField: '_id',
    foreignField: 'from'
})
//Post Hook (save) -> populate 'from' et 'to'
roleSchema.post('save', async function (roleDoc) {
    await roleDoc.populate('user').execPopulate()
})
roleSchema.plugin(require('mongoose-autopopulate'))

const Role = mongoose.model('Role', roleSchema)

module.exports = Role
