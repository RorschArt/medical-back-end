// Npm
const mongoose      = require('mongoose')
const ImageArticle  = require('./imageArticle')

const articleSchema = new mongoose.Schema({
        title: {
            type: String,
            trim: true,
            default: 'Nouveau Titre',
            required: true
        },
        subTitle: {
            type: String,
            trim: true,
            default: 'Nouveau Sous-Titre'
        },
        textBodyArticle: {
            type: String,
            trim: true,
            default: 'Nouveau Contenu',
            required: true
        },
        imageId: {
            type: mongoose.Schema.Types.ObjectId
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role',
            required: true,
            autopopulate: true
        }
    }, { timestamps: true }
)

articleSchema.virtual('comments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'articleFrom'
})
//Post Hook (save) -> populate author
articleSchema.post('save', async function(articlesDoc) {
    await articlesDoc.populate('author').execPopulate()
})
//Post Hook (findOneAndDelete) -> populate author
articleSchema.post('findOneAndDelete', async function(articleDoc) {
    await articleDoc.populate('author').execPopulate()
    await ImageArticle.delete({ _id: articleDoc.imageId })
})

articleSchema.plugin(require('mongoose-autopopulate'))

const Article = mongoose.model('Article', articleSchema)

module.exports = Article
