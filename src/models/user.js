// Npm
const mongoose      = require('mongoose')
const validator     = require('validator')
const bcrypt        = require('bcrypt')
const jwt           = require('jsonwebtoken')
// Models
const Role          = require('./role')
const Comment       = require('./comment')
const Activity      = require('../models/activity')
const Avatar        = require('../models/avatar')

const MailReceived  = require('../models/mail-received')
const MailSended    = require('../models/mail-sended')
const MailSaved     = require('../models/mail-saved')

const userSchema = new mongoose.Schema({
        email: {
            type: String,
            unique: true,
            required: true,
            trim: true,
            lowercase: true,
            validate(value) {
                if (!validator.isEmail(value)) {
                    throw new Error('Email is invalid')
                }
            }
        },
        pseudoName: {
            type: String,
            trim: true,
            default: 'Inconnu',
            required: true,
            unique: true
        },
        age: {
            type: Number,
            default: 0,
            required: true,
            validate(number) {
                if (number < 0) {
                    throw new Error('Erreur : Seul un nombre positif est possible !')
                }
            }
        },
        password: {
            type: String,
            trim: true,
            minlength: 6,
            required: true,
            validate(password) {
                switch (password.toLowerCase()) {
                    case 'password':
                        throw new Error('password ne peut pas contenir le mot password')
                        break;
                    case 'motdepasse':
                        throw new Error('password ne peut pas contenir le mot motdepasse')
                        break;
                }
            }
        },
        avatarId: {
              type: mongoose.Schema.Types.ObjectId
        },
        tokens: [{
            token: {
                type: String,
                required: true
            }
        }]
    }, { timestamps: true }
)

// N'existe pas dans la base (lien virtuel)
userSchema.virtual('role', {
    ref: 'Role',
    localField: '_id',
    foreignField: 'user',
    justOne: true
})
// Hashage du password avant enregistrement
userSchema.pre('save', async function() {
    // Impossible d'utiliser ES6 fonctions syntaxe
        const user = this

    // Effectuer le hashage seulement si l'utilisateur l'a updater
        if(user.isModified('password')) {
            user.password = await bcrypt.hash(user.password, 8)
        }
    })
// Populate après un findOne
userSchema.post('findOne', async function(userDoc) {
    await userDoc.populate('role').execPopulate()
})
// Supprime toutes les entités lors de la suppression de L'utilisateur
userSchema.statics.deleteAllEntitiesLinked = async (user) => {

await User.deleteOne({ _id: user._id })
await MailSended.deleteMany({ from: user.role._id })
await MailReceived.deleteMany({ to: user.role._id })
await MailSaved.deleteMany({ from: user.role._id })
await Activity.deleteMany({ owner: user.role._id })
await Role.deleteOne({ _id: user.role._id })
await Avatar.deleteOne({ _id: user.role.avatarId })
}
// Recherche de l'utilisateur et vérification mot de passe
userSchema.statics.findByCredentials = async (email, password) => {

    const user = await User.findOne({ email })

    if (!user) {
        throw new Error()
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error()
    }

    return user
}

// Génération JWT
userSchema.methods.generateAuthToken = async function () {
    // Mongoose nous donne accès à l'instance du modele (ES6 function impossible)
    const user = this
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET)

    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}

// Protection des données
    // Est appelé à chaque JSON Stringify
userSchema.methods.toJSON =  function () {
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens

    return userObject
}

userSchema.plugin(require('mongoose-autopopulate'))

const User = mongoose.model('User', userSchema)

module.exports = User
