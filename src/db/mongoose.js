// Npm
const mongoose = require('mongoose')
// Démarrage DB
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    keepAlive: true,
    reconnectTries: 30
})