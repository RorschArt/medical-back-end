const multer  = require('multer')
const express = require('express')
var cors = require('cors')
const app     = express()
// Connexion Base de Donnée
require('./db/mongoose')
// Port
const port    = process.env.PORT

            /*Routers files*/
// User
const userRouter         = require('./routers/user')
const roleRouter         = require('./routers/role')
const avatarRouter       = require('./routers/avatar')
// Article
const articleRouter      = require('./routers/article')
const articleImageRouter = require('./routers/article-image')
const commentRouter      = require('./routers/comment')
// Timetable
const activityRouter     = require('./routers/activity')
// Mail
const mailSavedRouter    = require('./routers/mail-saved')
const mailReceivedRouter = require('./routers/mail-received')
const mailSendedRouter   = require('./routers/mail-sended')
const mailRouter         = require('./routers/mail')
// JSON Format
app.use(express.json())
app.use(cors())
            /* Routers set pour Express */
// User
app.use(userRouter)
app.use(roleRouter)
app.use(avatarRouter)
// Article
app.use(articleRouter)
app.use(articleImageRouter)
app.use(commentRouter)
// Timetable
app.use(activityRouter)
// Mail
app.use(mailSavedRouter)
app.use(mailReceivedRouter)
app.use(mailSendedRouter)
app.use(mailRouter)
//====================== Maintenance Mode ====================================
// A décommenter si on veux désactiver tous les routers
    // app.use((req, res, next) => {
    //     res.status(503).send('Service en Maintenance, svp réessayez plus tard')
    // })
//=============================================================================

// Ecoute du serveur
app.listen(port, () => {
    console.log('Serveur en écoute sur port :' ,port)
})
