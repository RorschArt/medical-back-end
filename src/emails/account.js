const sgMail          = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomEmail = (email, name) => {

    const msg = {
        to: email,
        subject: 'Bienvenue au groupe médical ! 🧘 💌',
        from: 'axeldaguerredev@gmail.com',
        text: `Bonjour ${name}, ravie de vous compter parmis notre groupe médical !\n
                Nous espérons que vous apprécierez notre nouveau site web. Vous pourrez envoyer des emails\
                à votre consultant, voir les dates disponibles pour votre prise de rendez-vous.\n
                Ce site à été spécialement conçu pour faciliter le suivi de nos patients et de leur donner toute\
                l'attention qu'ils méritent. Vous n'êtes plus seul.\n
                Le site web ne demande qu'à grandir, n'hésitez pas à faire part de vos suggestion et de donner\
                votre avis pour améliorer l'expérience de ce site web.\n
                Nous vous souhaitons une bienvenu chez nous et espérons que l'expérience sera la meilleure possible.\
                Le Groupe Médical. 👨‍⚕️👩‍⚕️🏥`
    }
    sgMail.send(msg)
}

const sendCancelationEmail = (email, name) => {

    const msg = {
        to: email,
        from: 'axeldaguerredev@gmail.com',
        subject: `C\'est si Triste de Te Voir Partir ${name} ! 🥺`,
        text: `C'est dommage de vous voir partir ${name} !
        Mais sache que vous pouvez recréer un compte quand vous le souhaitez.
        Le Groupe Médical. 👨‍⚕️👩‍⚕️🏥`
    }
    sgMail.send(msg)
}

module.exports = {
    sendWelcomEmail,
    sendCancelationEmail
}
