const auth         = require('../Middlewares/authentification')
const express      = require('express')
const router       = new express.Router()
// Models
const MailReceived = require('../models/mail-received')

            /* Routers */
// READ Mails (Recus) (limit skip sortBy)
router.get('/mails/received', auth, async (req, res) => {

    const queryParams    = {}
    const sort           = {}

    if (req.query.sortBy) {
        const parts      = req.query.sortBy.split(':')
        sort[parts[0]]   = parts[1] === 'desc' ? -1 : 1
        queryParams.sort = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const mails = await MailReceived.find({ to: req.user.role._id }, null, queryParams)
        await MailReceived.updateMany( { to:req.user.role._id }, { unRead:false } )
        res.send(mails)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Mails Reçus Counter
router.get('/mails/received/counter', auth, async (req, res) => {

    try {
        const mails = await MailReceived.find({ to: req.user.role._id })
        res.send({count: mails.length})
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Mail Reçu
router.get('/mails/received/:id', auth, async (req, res) => {

    try {
        const mail = await MailReceived.findOne({ _id: req.params.id })

        if(!mail) {
            return res.status(404).send()
        }

        res.send(mail)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Mails (reçus) NON lu
router.get('/mails/unread', auth, async (req, res) => {
    try {

        const mails = await MailReceived.find({ to: req.user.role._id, unRead: true })

        res.send(mails)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Mails (reçus) ALL
router.delete('/mails/received/all', auth, async (req, res) => {

    try {
        const mailsDeleted = await MailReceived.deleteMany({ to: req.user.role._id})

        res.send({ mailsDeleted })
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Mail (reçus)
router.delete('/mails/received/:id', auth, async (req, res) => {

    try {
        const mailDeleted = await MailReceived.findOneAndDelete({ _id: req.params.id, to: req.user.role._id })

        if (!mailDeleted) {
            return res.status(400).send()
        }

        res.send(mailDeleted)
    } catch (e) {
        res.status(401).send(e)
    }
})

module.exports = router
