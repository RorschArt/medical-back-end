const auth    = require('../Middlewares/authentification')
const express = require('express')
const router  = new express.Router()
// Models
const Article = require('../models/article')

            /* Routers */
// CREATE Article
router.post('/articles', auth, async (req, res) => {

    const rolesAuthorized  = ['admin']
    const isAuthorizedUser = [req.user.role.roleName].every((role) => rolesAuthorized.includes(role))

    const article = new Article({
        ...req.body,
        author: req.user.role._id
    })

    try {
        if (!isAuthorizedUser) {
            return res.status(401).send()
        }

        await article.save()

        res.status(201).send(article)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Articles (limit skip sortBy)
router.get('/articles', async (req, res) => {

    const queryParams    = {}
    const sort           = {}

    if (req.query.sortBy) {
        const parts      = req.query.sortBy.split(':')
        sort[parts[0]]   = parts[1] === 'desc' ? -1 : 1
        queryParams.sort = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const articles = await Article.find( null, null, queryParams)

        res.send(articles)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Article
router.get('/articles/:id', async (req, res) => {

    try {
        const article = await Article.findOne({ _id:req.params.id })

        if(!article) {
            return res.status(404).send()
        }

        await article.populate('comments').execPopulate()

        res.send({ article, comments: article.comments })
    } catch (e) {
        res.status(500).send(e)
    }
})
// UPDATE Article
router.patch('/articles/:id', auth, async (req, res) => {

    const rolesAuthorized  = ['admin']
    const isAuthorizedUser = [req.user.role.roleName].every((role) => rolesAuthorized.includes(role))

    const updates          = Object.keys(req.body)
    const updatesAllowed   = [ 'subTitle', 'title', 'textBodyArticle', 'imageUrl']
    const isValidUpdates   = updates.every((update) => updatesAllowed.includes(update))

    if (!isAuthorizedUser) {
        return res.status(401).send()
    }

    if (!isValidUpdates) {
        return res.status(403).send()
    }

    try {

        const articleUpdated = await Article.findOneAndUpdate(
            { _id: req.params.id },
            req.body ,
            { new: true }
        )

        if (!articleUpdated) {
            return res.status(404).send()
        }

        res.send(articleUpdated)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Article
router.delete('/articles/:id', auth, async (req, res) => {

    try {

        const articleDeleted = await Article.findOneAndDelete({ _id: req.params.id })

        if(!req.user.isAdmin) {
            return res.status(401).send()
        }

        if (!articleDeleted) {
            return res.status(404).send()
        }

        res.send(articleDeleted)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router
