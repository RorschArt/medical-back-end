const auth         = require('../Middlewares/authentification')
const express      = require('express')
const router       = new express.Router()
// Models
const Role         = require('../models/role')
const MailSended   = require('../models/mail-sended')
const MailReceived = require('../models/mail-received')

            /* Routers */
// CREATE Mail
router.post('/mails/:id', auth, async (req, res) => {

    const mailSe = new MailSended({
        ...req.body,
        from: req.user.role._id,
        to: req.params.id
    })

    const mailRe = new MailReceived({
        ...req.body,
        from: req.user.role._id,
        to: req.params.id
    })

    try {
    // S'assurer que le destinataire existe
        const receiver = await Role.findOne({ _id: req.params.id })

        if (!receiver) {
            return res.status(404).send()
        }

        const mailSended = await mailSe.save()

        await mailRe.save()

        res.status(201).send(mailSended)
    } catch (e) {
        res.status(404).send(e)
    }
})

module.exports = router
