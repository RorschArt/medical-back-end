const auth    = require('../Middlewares/authentification')
const express = require('express')
const router  = new express.Router()
// Model
const Article = require('../models/article')
const Comment = require('../models/comment')

            /* Routers */
// CREATE Comment
router.post('/articles/comments/:idArticle', auth, async (req, res) => {

    try {
        const article = await Article.findById(req.params.idArticle)

        const comment = new Comment({
            ...req.body,
            author: req.user.role._id,
        // Throw Error quand article n'existe pas. Ne pas Sortir du try/catch
            articleFrom: article._id
        })

        const commentSaved = await comment.save()

        res.status(201).send(commentSaved)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Comments (skip limit)
router.get('/articles/comments/:idArticle', async (req, res) => {
  try {

    const queryParams    = {}
    const sort           = {}

    if (req.query.sortBy) {
        const parts      = req.query.sortBy.split(':')
        sort[parts[0]]   = parts[1] === 'desc' ? -1 : 1
        queryParams.sort = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit) 
        queryParams.skip  = parseInt(req.query.skip)
    }

        const article  = await Article.findById(req.params.idArticle)
        const comments = await Comment.find( { articleFrom: req.params.idArticle }, null, queryParams)

        if (!comments || !article) {
            return res.status(404).send()
        }

        res.send(comments)
    } catch (e) {
        res.status(500).send()
    }
})
// UPDATE Comment
router.patch('/articles/comments/:idComment', auth, async (req, res) => {

    const updates        = Object.keys(req.body)
    const updatesAllowed = ['textBodyComment']
    const isValidUpdates = updates.every((update) => updatesAllowed.includes(update))

    try {
        if (!isValidUpdates) {
            return res.status(403).send()
        }

        const commentUpdated = await Comment.findOneAndUpdate({
            _id: req.params.idComment,
            author: req.user.role._id
        }, req.body)

        if (!commentUpdated) {
            return res.status(404).send()
        }
        res.send(commentUpdated)
    } catch (e) {
        res.status(500).send(e)
    }
})
//DELETE Comment (ADMIN)
router.delete('/articles/comments/:idComment', auth, async (req, res) => {

    try {
        if (!req.user.isAdmin) {
            return res.status(401).send()
        }

        const commentDeleted = await Comment.findOneAndDelete({_id: req.params.idComment})

        if (!commentDeleted) {
            res.status(400).send()
        }

        res.send(commentDeleted)
    } catch (e) {
        res.status(500).send()
    }
})
//DELETE Comments (ADMIN)
router.delete('/articles/comments/all', auth, async (req, res) => {

    try {
        if (!req.user.isAdmin) {
            return res.status(401).send()
        }

        const commentDeleted = await Comment.findOneAndDelete({ _id: req.params.idComment })

        if (!commentDeleted) {
            res.status(400).send()
        }

        res.send(commentDeleted)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router
