const auth      = require('../Middlewares/authentification')
const express   = require('express')
const router    = new express.Router()
// Models
const Role      = require('../models/role')
const MailSaved = require('../models/mail-saved')

            /* Routers */
// SAVE Mail AVEC destinataire (:id)
router.post('/mails/save/:id', auth, async (req, res) => {

    const mailSaved = new MailSaved({
        ...req.body,
        from: req.user.role._id,
        to: req.params.id
    })

    try {
        const receiver = await Role.findOne({ _id: req.params.id })

        if (!receiver) {
            return res.status(404).send()
        }

         await mailSaved.save()

        res.status(201).send(mailSaved)
    } catch (e) {
        res.status(400).send(e)
    }
})
// SAVE Mail SANS destinataire
router.post('/mails/save', auth, async (req, res) => {

    const mailSaved = new MailSaved({
        ...req.body,
        from: req.user.role._id,
    })

    try {
         await mailSaved.save()

        res.status(201).send(mailSaved)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Mails (Enregistré)
router.get('/mails/saved', auth, async (req, res) => {

    const queryParams     = {}
    const sort            = {}

    if (req.query.sortBy) {
        const parts       = req.query.sortBy.split(':')
        sort[parts[0]]    = parts[1] === 'desc' ? -1 : 1
        queryParams.sort  = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const mails = await MailSaved.find({ from: req.user.role._id}, null, queryParams)

        res.send(mails)
    } catch (e) {
        res.status(500).send(e)
    }
})

// READ Mail (Enregistré)
router.get('/mails/saved/:idMail', auth, async (req, res) => {

    try {
        const mail = await MailSaved.findOne({ from: req.user.role._id, _id: req.params.idMail })

        res.send(mail)
    } catch (e) {
        res.status(500).send(e)
    }
})
// UPDATE Mail (Saved)
router.patch('/mails/saved/:id', async (req,res) => {

    const updates        = Object.keys(req.body)
    const updatesAllowed = ['textBody', 'header']
    const isValidUpdates = updates.every((update) => updatesAllowed.includes(update))

    if (!isValidUpdates) {
        return res.status(403).send()
    }

    try {
        const mailUpdated = await MailSaved.findOneAndUpdate({ _id: req.params.id }, req.body , { new: true, runValidators: true })

        if (!mailUpdated) {
            return res.status(404).send()
        }

        res.send(mailUpdated)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Mails (Enregistrés) ALL
router.delete('/mails/saved/all', auth, async (req, res) => {

    try {
        const mailsDeleted = await MailSaved.deleteMany({ })

        res.send({ mailsDeleted})
    } catch (e) {
        res.status(401).send(e)
    }
})
// DELETE Mail (Enregistré)
router.delete('/mails/saved/:id', auth, async (req, res) => {

    try {
        const mailDeleted = await MailSaved.findOneAndDelete({ _id: req.params.id, from: req.user.role._id })

        if (!mailDeleted) {
            return res.status(400).send()
        }

        res.send(mailDeleted)
    } catch (e) {
        res.status(401).send(e)
    }
})

module.exports = router
