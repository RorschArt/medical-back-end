const auth = require('../Middlewares/authentification')
const express = require('express')
const router = new express.Router()
// Models
const User = require('../models/user')
const Role = require('../models/role')
const Avatar = require('../models/avatar')
// SendGrid (email)
const { sendWelcomEmail, sendCancelationEmail } = require('../emails/account')

            /* Routers */
// CREATE User (guest)
router.post('/users', async (req, res) => {
    try {
        const userCreated = await new User(req.body).save()

        sendWelcomEmail(userCreated.email, userCreated.pseudoName)
        const token = await userCreated.generateAuthToken()
        const role = new Role({ roleName: 'guest', user: userCreated._id })

        const user = await role.save()

        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})
// LOGIN User
router.post('/users/login', async (req, res) => {

    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()

        await user.save()

        res.send({ user: user.role, token })
    } catch (e) {
        res.status(400).send()
    }
})
// LOGOUT User
router.post('/users/logout', auth, async (req, res) => {

    try {
        req.user.tokens = req.user.tokens.filter(token => token.token !== req.token)

        await req.user.save()

        res.send(req.user.role)
    } catch (e) {
        res.status(500).send()
    }
})
// LOGOUT All
router.post('/users/logoutAll', auth, async (req, res) => {

    try {
        req.user.tokens = []

        await req.user.save()

        res.send(req.user.role)
    } catch (e) {
        res.status(500).send()
    }
})
// READ Profile
router.get('/users/me', auth, async (req, res) => {

    try {
        res.send(req.user.role)
   } catch (e) {
    res.status(500).send(e)
   }
})

// READ User
router.get('/users/:id', auth, async (req, res) => {

    try {
        const user = await User.findOne({ _id: req.params.id })

        if (!user) {
            return res.status(404).send()
        }

        res.send(user.role)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ User PSEUDONAME
router.get('/users/find/:pseudoName', auth, async (req, res) => {

    try {
        const pseudoName = req.params.pseudoName
  // Pseudo Unique
        const user = await User.findOne({ pseudoName })

        if (!user) {
            return res.status(404).send()
        }

        res.send(user.role)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Users Depuis Guest
router.get('/users', async (req, res) => {

    const queryParams = {}
    const sort        = {}

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
        queryParams.sort = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip = parseInt(req.query.skip)
    }

    try {
        const users = await Role.find( null, null, queryParams)

        res.send(users)
    } catch (e) {
        res.status(400).send(e)
    }
})
// UPDATE User/me
router.patch('/users/me', auth, async (req, res) => {

    const updates = Object.keys(req.body)
    const updatesAllowed = ['age', 'pseudoName', 'email', 'password']
    const isValidUpdates = updates.every((update) => updatesAllowed.includes(update))

    if (!isValidUpdates) {
        return res.status(403).send()
    }

    try {
        updates.forEach((update) => req.user[update] = req.body[update])

        const user = await req.user.save()

        res.send(user.role)
    } catch (e) {
        res.status(400).send()
    }
})
// UPDATE User/:id (Admin)
router.patch('/users/:id', auth, async (req, res) => {

    const updates = Object.keys(req.body)
    const updatesAllowed = ['age', 'pseudoName', 'email', 'password']
    const isValidUpdates = updates.every((update) => updatesAllowed.includes(update))

    if (!isValidUpdates) {
        res.status(403).send()
    }

    try {
        updates.forEach((update) => req.user[update] = req.body[update])

        await req.user.save()

        res.send(req.user)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE User/me
router.delete('/users/me', auth, async (req, res) => {

    try {
        await req.user.remove()
        await User.deleteAllEntitiesLinked(req.user)

        sendCancelationEmail(req.user.email, req.user.pseudoName)

        res.send(req.user.role)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE User/:id (Admin)
router.delete('/users/:id', auth, async (req, res) => {

    try {
        const user = await User.findOne({ _id: req.params.id })

        if (!req.user.isAdmin) {
            return res.status(401).send()
        }

        if (!user) {
            return res.status(404).send()
        }

        sendCancelationEmail(user.email, user.pseudoName)
         await User.deleteAllEntitiesLinked(user)


        res.send(user)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router
