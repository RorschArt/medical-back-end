// Middleware
const auth = require('../Middlewares/authentification')
// Express
const express = require('express')
const router = new express.Router()
// Models
const Timetable = require('../models/timetable')
const Activity = require('../models/activity')

                /* Routers */
// CREATE Timetable
router.post('/timetables', auth, async (req, res) => {
//TODO: refactoriser tout ces isAuthorized

    try {
        if (!isAuthorizedUser) {
            return res.status(401).send()
        }
        const activity = new Activity ( { name: 'Nouvelle Activité' })
        const timetable = new Timetable({
            ...req.body,
            owner: req.user.role._id,
            activities: [{ activity: activity._id  }]
        })

        await activity.save()
        const timetableSaved = await timetable.save()

        res.status(201).send(timetableSaved)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Timetables ALL
router.get('/timetables', auth, async (req, res) => {

    try {
        const timetables = await Timetable.find({ })

        res.send(timetables)
    } catch (e) {
        res.status(500).send(e)
    }
})
// READ Timetables MY
router.get('/timetables/my', auth, async (req, res) => {

    try {
        const timetable = await Timetable.findOne({ owner: req.user.role._id})

        res.send(timetable)
    } catch (e) {
        res.status(500).send(e)
    }
})
//READ Timetable :id
router.get('/timetables/:id', auth, async (req, res) => {

    try {
        const timetable = await Timetable.findById(req.params.id)

        if(!timetable) {
            return res.status(404).send()
        }

        res.send(timetable)
    } catch (e) {
        res.status(500).send(e)
    }
})
//DELETE Timetable :id (SuperAdmin)
router.delete('/timetables/:id', async (req, res) => {

    try {
        const timetableDeleted = await Timetable.findByIdAndDelete(req.params.id)

        if (!timetableDeleted) {
            return res.status(404).send({ })
        }

        res.send(timetableDeleted)
    } catch (error) {
        res.status(500).send(timetableDeleted)
    }
})
module.exports = router
