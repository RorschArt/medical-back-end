const auth     = require('../Middlewares/authentification')
const express  = require('express')
const router   = new express.Router()
const Activity = require('../models/activity')

                /* Routers */
// CREATE Activity
router.post('/timetables/my/activities', auth, async (req, res) => {

    try {
        if (!req.user.isAdmin) {
            return res.status(401).send()
        }

        const activity = new Activity( {
            ...req.body,
            owner: req.user.role._id
        })

        await activity.save()

        res.status(201).send(activity)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Activities My
router.get('/timetables/my/activities', auth, async (req, res) => {

    const queryParams     = {}
    const sort            = {}

    // permet de créer par ex sort = createdAt : -1
    if (req.query.sortBy) {
        // ['createdAt', 'desc' ]
        const parts       = req.query.sortBy.split(':')
        // sort: createdAt : -1 ou 1
        sort[parts[0]]    = parts[1] === 'desc' ? -1 : 1
        queryParams.sort  = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const activities  = await Activity.find({ owner: req.user.role._id}, null, queryParams)

        res.send(activities)
    } catch (e) {
        res.status(400).send()
    }
})
// READ Activities :id owner  (limit skip sortBy asc desc)
router.get('/timetables/activities/:idOwner', auth, async (req, res) => {

    const queryParams     = {}
    const sort            = {}

    if (req.query.sortBy) {
        const parts       = req.query.sortBy.split(':')
        sort[parts[0]]    = parts[1] === 'desc' ? -1 : 1
        queryParams.sort  = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit) || 10
        queryParams.skip  = parseInt(req.query.skip) || 0
    }

    try {
        const activities = await Activity.find({ owner: req.params.idOwner }, null, queryParams)

        if (!activities) {
            return res.status(400).send()
        }

         res.send(activities)
    } catch (e) {
        res.status(500).send()
    }
})
// DELETE Activity
router.delete('/timetables/my/activities/:id', auth, async (req, res) => {

    try {
        const activityDeleted = await Activity.findOneAndDelete({
            _id: req.params.id,
            owner: req.user.role._id
        })

        if (!activityDeleted) {
            return res.status(400).send()
        }

        res.send(activityDeleted)
    } catch (e) {
        res.status(500).send()
    }
})
// DELETE Activities
router.delete('/timetables/my/activities', auth, async (req, res) => {

    try {
        if (!req.user.isAdmin) {
            return res.status(401).send()
        }

        const activitiesDeleted = await Activity.deleteMany({ owner: req.user.role._id })

        if (!activitiesDeleted) {
            return res.status(400).send()
        }

        res.send(activitiesDeleted)
    } catch (e) {
        res.status(500).send()
    }
})
//Update Activity
router.patch('/timetables/my/activities/:id', auth, async (req, res) => {

    const updates        = Object.keys(req.body)
    const updatesAllowed = ['timestamp', 'name']
    const isValidUpdates = updates.every((update) => updatesAllowed.includes(update))

    if (!isValidUpdates) {
        return res.status(403).send()
    }

    try {

        const activity = await Activity.findOneAndUpdate({_id: req.params.id, owner: req.user.role._id }, req.body)

        res.send(activity)
    } catch (e) {
        res.status(400).send(e)
    }
})

module.exports = router
