const multer  = require('multer')
const sharp   = require('sharp')
const auth    = require('../Middlewares/authentification')
const express = require('express')
const router  = new express.Router()
// Models
const User    = require('../models/user')
const Avatar    = require('../models/avatar')
// Multer instances
                // Avatar
const avatar = multer({
    limits: {
        fileSize: 500000
    },
    fileFilter(req, file, cb){
        if (!file.originalname.match(/\.(jpg|png|jpeg)$/)) {
            return cb(new Error('Veuillez Uploader un fichier image SVP !'))
        }
        cb(undefined, true)
    }
})

            /* Routers */
// CREATE Avatar me
// pour handle une erreur d'un middleware express a besoin d'une callback en dernier argument
// avec 4 arguments (error, req, res, next) sinon express ne comprend pas qu'il s'agit d'un catch
router.post('/users/me/avatar', auth, avatar.single('avatar'), async (req, res) => {
  const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()

  if (!req.user.avatarId) {
    const newAvatar = await new Avatar()

    newAvatar.avatarImage = buffer
    req.user.avatarId = newAvatar._id

    await req.user.save()
    await newAvatar.save()

    return res.status(201).send({ idAvatar: newAvatar._id, updatedAt: newAvatar.updatedAt })
  }

    const avatarUpdated = await Avatar.findOneAndUpdate({ _id: req.user.avatarId },
      { avatarImage: buffer },
      { new: true }
    )

    return res.status(201).send({ idAvatar: avatarUpdated._id, updatedAt: avatarUpdated.updatedAt })
}, (error, req, res, next) => res.status(400).send({ error: error.message })
)
// READ Avatar ALL
router.get('/users/:idUser/avatar', async (req, res) => {

    try {
        const user = await User.findOne({ _id: req.params.idUser })
        const avatar = await Avatar.findOne({ _id: user.avatarId })

        if (!avatar) {
            throw new Error()
        }
        res.set({
          'Content-Type': 'image/png',
          'Cache-Control': 'no-cache'
        })

        res.send(avatar.avatarImage)
    } catch (e) {
        res.status(404).send()
    }
})
// READ Avatar ME
router.get('/users/:idAvatar/avatar/:hash*?', async (req, res) => {

    try {
        const avatar = await Avatar.findOne({ _id: req.params.idAvatar })

        if (!avatar) {
            throw new Error()
        }
        res.set({
          'Content-Type': 'image/png',
          'Cache-Control': 'no-cache'
        })

        res.send(avatar.avatarImage)
    } catch (e) {
        res.status(404).send()
    }
})
// DELETE Avatar me
router.delete('/users/me/avatar', async (req, res) => {

    req.user.avatar = undefined
    await req.user.save()

    res.send()
})


module.exports = router
