const multer  = require('multer')
const sharp   = require('sharp')
const auth    = require('../Middlewares/authentification')
const express = require('express')
const router  = new express.Router()
// Models
const Article = require('../models/article')
const ImageArticle = require('../models/imageArticle')
// Multer Instance
const image = multer({
    limits: {
        fileSize: 6000000
    },
    fileFilter(req, file, cb){
        if (!file.originalname.match(/\.(jpg|png|jpeg)$/)) {
            return cb(new Error('Veuillez Uploader un fichier image SVP !'))
        }
        cb(undefined, true)
    }
})

            /* Routers */
// CREATE Image Article
router.post('/articles/image', auth, image.single('imageArticle'), async (req, res) => {
    const rolesAuthorized  = ['admin']
    const isAuthorizedUser = [req.user.role.roleName].every((role) => rolesAuthorized.includes(role))

    try {
        if (!isAuthorizedUser) {
            return res.status(401).send()
        }
        const buffer = await sharp(req.file.buffer).resize({ width: 1200, height: 675 }).toFormat('jpeg').toBuffer()
        const imageArticle = await new ImageArticle({ image: buffer})

        imageArticle.image = buffer
        const imageArticleSaved = await imageArticle.save()

        res.status(201).send(imageArticleSaved)
    } catch (e) {
        res.status(406).send()
    }
}, (error, req, res, next) => res.status(400).send(error)
)
// READ Image Article
router.get('/articles/:imageArticle/image', async (req, res) => {

    try {
        const imageArticle = await ImageArticle.findOne({ _id: req.params.imageArticle })

        if (!imageArticle || !imageArticle.image) {
            throw new Error()
        }

        res.set('Content-Type', 'image/jpg')

        res.send(imageArticle.image)
    } catch (e) {
        res.status(404).send()
    }
})
// DELETE Image Article
router.delete('/articles/:idArticle/image', auth, async (req, res) => {

    const rolesAuthorized  = ['admin']
    const isAuthorizedUser = [req.user.role.roleName].every((role) => rolesAuthorized.includes(role))

    try {
        if (!isAuthorizedUser) {
            return res.status(401).send()
        }

        const article = await Article.findOne({ _id: req.params.idArticle })

        article.image = undefined
        await article.save()

        res.send()
    } catch (e) {
        res.status(400).send(e)
    }
})

module.exports = router
