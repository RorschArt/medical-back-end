const auth    = require('../Middlewares/authentification')
const express = require('express')
const router  = new express.Router()
// Models
const Role    = require('../models/role')
const User    = require('../models/user')

            /* Routers */
// CREATE User (Admin DEV ONLY)
router.post('/users/admin', async (req, res) => {

    const userCreated = new User(req.body)
    const role = new Role({ roleName: 'admin', user: userCreated._id })

    try {
        const token = await userCreated.generateAuthToken()

        const user = await role.save()

        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})
// UPDATE to Admin a Guest from another Admin
router.patch('/roles/admins/:id', auth, async (req, res) => {

    if (!req.user.isAdmin) {
        return res.status(401).send()
    }

    try {
        const user = await User.findOne({ _id: req.params.id })
        user.role.roleName = 'admin'

        const userUpdated = await user.role.save()

        res.status(201).send( userUpdated )
    } catch (e) {
       res.status(400).send()
    }
})
// GET Roles Admin (limit skip sortBy)
router.get('/roles/admins/admin', async (req, res) => {

    const queryParams     = {}
    const sort            = {}

    if (req.query.sortBy) {
        const parts       = req.query.sortBy.split(':')
        sort[parts[0]]    = parts[1] === 'desc' ? -1 : 1
        queryParams.sort  = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const admins = await Role.find({roleName: 'admin'}, null, queryParams)

        res.send(admins)
    } catch (e) {
        res.status(400).send(e)
    }
})
// GET Roles Guest (limit skip)
router.get('/roles/guests', async (req, res) => {

    const queryParams     = {}
    const sort            = {}

    if (req.query.sortBy) {
        const parts       = req.query.sortBy.split(':')
        sort[parts[0]]    = parts[1] === 'desc' ? -1 : 1
        queryParams.sort  = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const admins = await Role.find({roleName: 'guest'}, null, queryParams)

        res.send(admins)
    } catch (e) {
        res.status(400).send(e)
    }
})

module.exports = router
