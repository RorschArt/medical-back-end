// Middleware
const auth       = require('../Middlewares/authentification')
// Express
const express    = require('express')
const router     = new express.Router()
// Models
const MailSended = require('../models/mail-sended')

            /* Routers */
// READ Mails (Envoyé) (limit skip sortBy)
router.get('/mails/sended', auth, async (req, res) => {

    const queryParams    = {}
    const sort           = {}

    if (req.query.sortBy) {
        const parts      = req.query.sortBy.split(':')
        sort[parts[0]]   = parts[1] === 'desc' ? -1 : 1
        queryParams.sort = sort
    }

    if (req.query.limit || req.query.skip) {
        queryParams.limit = parseInt(req.query.limit)
        queryParams.skip  = parseInt(req.query.skip)
    }

    try {
        const mails = await MailSended.find({ from: req.user.role._id }, null, queryParams)

        res.send(mails)
    } catch (e) {
        res.status(400).send(e)
    }
})
// READ Mail (Envoyé)
router.get('/mails/sended/:idMail', auth, async (req, res) => {

    try {
        const mail = await MailSended.findOne({ from: req.user.role._id, _id: req.params.idMail })

        res.send(mail)
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Mails (envoyés) ALL
router.delete('/mails/sended/all', auth, async (req, res) => {

    try {
        const mailsDeleted = await MailSended.deleteMany({ })

        res.send({ mailsDeleted })
    } catch (e) {
        res.status(500).send(e)
    }
})
// DELETE Mail (envoyé)
router.delete('/mails/sended/:id', auth, async (req, res) => {

    try {
        const mailDeleted = await MailSended.findOneAndDelete({ _id: req.params.id, from: req.user.role._id })

        if (!mailDeleted) {
            return res.status(400).send()
        }

        res.send(mailDeleted)
    } catch (e) {
        res.status(401).send(e)
    }
})

module.exports = router