# Back-End de l'application du group médical

## URL du serveur de production 🖥️

https://daguerre-medical-manager.herokuapp.com

## Voir la liste du Crud (Postman) 📡

[Lien Postman](https://documenter.getpostman.com/view/7640480/SVSKLoaE)

Se mettre en environnement `Medical-manager (PROD)`

## Wiki du Backend

[Wiki Main Page](https://gitlab.com/AxelDaguerre/medical-back-end/wikis/Main-page)

[Slide de mon projet](https://slides.com/axelaguerred/medical-project)

[Rapport de stage](https://gitlab.com/AxelDaguerre/my-documentation/blob/master/Stage/Project.md)